/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope Public
 */

/**
 * Author: Emerson Winchester
 * Development Date : December, 2018
 * Purpose: Script assigns the correct Tax Code on the line item, and looks up the correct tax rate via Custom Record. 
*/

define(['N/record','N/log','N/search'], function(record,log,search) {

	var defaults = {
		dieselItemId: ['15'],
		regularItemId: ['16','17','5'],
		nonFuelId: ['7','8'],
		caStateId: 4,
		txStateId: 44,
		taxCodes: {
			ca_regular: 27,
			ca_diesel: 28,
			ca_non_fuel: 29,
			tx_regular: 33,
			tx_diesel: 34,
			tx_non_fuel: 32
		},
		isCreate: false
	}

	function beforeSubmit(ctx) {
		try {

			var r = ctx.newRecord
			if (ctx.type == 'create') {defaults.isCreate = true}
			var startingAmount = r.getValue('total')
			var zips = getServiceZips(r)

			var taxRates = taxRatesByZip(zips)
			setTaxFields(r,taxRates)

			var endingAmount = r.getValue('total')
			log.audit({title:'starting amount:' + startingAmount+ ', ending amount: '+endingAmount, details: 'none'})
			if(startingAmount - endingAmount != 0) {
				log.audit({title:'shit',details:'calculated incorrectly'})
			}
			log.audit({
				title:'subtotal',
				details: r.getValue('subtotal')
			})

			log.audit({
				title:'tax amount',
				details: r.getValue('taxtotal')
			})
			log.audit({
				title:'calced total',
				details: r.getValue('subtotal') + r.getValue('taxtotal')
			})

		}
		catch(e) {
			log.error({
				title: e.name,
				details: e.message
			})
		}
	}

	function taxRatesByZip(zips) {

		try {
			var taxRates = {}

			zips.forEach(function(zipToSearch) {
				var customrecord_tax_ratesSearchObj = search.create({
				   	type: "customrecord_tax_rates",
				   	filters:
				   	[
				      ["isinactive","is","F"], 
				      "AND", 
				      ["custrecord_zip_code","is",zipToSearch]
				   	],

				   	columns:
				   	[
				   		search.createColumn({
				   			name: "internalid",
				   			sort: search.Sort.ASC,
				   			label: "Internal Id",
				   		}),
						search.createColumn({name: "name", label: "Name"}),
						search.createColumn({name: "custrecord_tax_state", label: "State"}),
						search.createColumn({name: "custrecord_zip_code", label: "Zip Code"}),
						search.createColumn({name: "custrecord_tax_regular_rate", label: "Regular/Premium Rate"}),
						search.createColumn({name: "custrecord_tax_diesel_rate", label: "Diesel Rate"}),
						search.createColumn({name: "custrecord_tax_non_fuel_rate", label: "Non Fuel Rate"})
				   	]
				});
				var searchResultCount = customrecord_tax_ratesSearchObj.runPaged().count;
				if (searchResultCount == 0) {
					//send email to kaelyn
				}

				customrecord_tax_ratesSearchObj.run().each(function(result){

					var zip = result.getValue({name: 'custrecord_zip_code'})
					var reg = result.getValue({name: 'custrecord_tax_regular_rate'}).slice(0,-1)
					var diesel = result.getValue({name: 'custrecord_tax_diesel_rate'}).slice(0,-1)
					var nonfuel = result.getValue({name: 'custrecord_tax_non_fuel_rate'}).slice(0,-1)

					if(!taxRates.hasOwnProperty(zip)) {
						taxRates[zip] = {
							regular: reg,
							diesel: diesel,
							nonfuel: nonfuel
						}
					}
					return true;
				});
			})
			return taxRates
		}
		catch(e) {
			log.error({
				title: e.name,
				details: e.message
			})
		}
	}

	function setTaxFields(rec,taxInfo) {
		var lineCount = rec.getLineCount('item')
		var state = rec.getValue('shipstate')

		for (var a = 0; a < lineCount; a++) {
			var item = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'item',
				line: a
			})
			var amount = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_tax_included_amt',
				line: a
			})
			var zip = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_service_zip',
				line: a
			})
			//if the current line item being looped is a taxable item #, then set tax code and tax rate
			if (defaults.regularItemId.indexOf(item) > -1 || defaults.dieselItemId.indexOf(item) > -1 || defaults.nonFuelId.indexOf(item) > -1) {
				setTaxCode(state,item,rec,a)
				setTaxRate(item,taxInfo,rec,a,amount,zip)
			}
		}
	}

	function setTaxCode(state,item,rec,line) {

		//if REGULAR / PREMIUM
		if (defaults.regularItemId.indexOf(item) > -1) {
			if (state == 'CA') {
				setLineValue(rec,line,'taxcode',defaults.taxCodes.ca_regular)
			}

			if (state == 'TX') {
				setLineValue(rec,line,'taxcode',defaults.taxCodes.tx_regular)
			}
			
		}

		//if DIESEL
		if (defaults.dieselItemId.indexOf(item) > -1) {
			if (state == 'CA') {
				setLineValue(rec,line,'taxcode',defaults.taxCodes.ca_diesel)
			}

			if (state == 'TX') {
				setLineValue(rec,line,'taxcode',defaults.taxCodes.tx_diesel)
			}
		}

		//if some NON-FUEL item
		if (defaults.nonFuelId.indexOf(item) > -1) {
			if (state == 'CA') {
				setLineValue(rec,line,'taxcode',defaults.taxCodes.ca_non_fuel)
			}

			if (state == 'TX') {
				setLineValue(rec,line,'taxcode',defaults.taxCodes.tx_non_fuel)
			}
		}
	}

	function setTaxRate(item,taxInfo,rec,line,amount,zip) {
		if (defaults.regularItemId.indexOf(item) > -1) {
			var rate = amount / (1 + (taxInfo[zip].regular / 100))
			setLineValue(rec,line,'amount',rate)
			setLineValue(rec,line,'taxrate1',taxInfo[zip].regular)

			//ADD LOGIC HERE:
			//IF THE TOTAL AFTER TAX IS CALCULATED != ORIGINAL AMOUNT, ADD OR SUBTRACT FROM AMOUNT COLUMN TO TRUE UP
		}
		
		if (defaults.dieselItemId.indexOf(item) > -1) {
			var rate = amount / (1 + (taxInfo[zip].diesel / 100))
			setLineValue(rec,line,'amount',rate)
			setLineValue(rec,line,'taxrate1',taxInfo[zip].diesel)
		}
		if (defaults.nonFuelId.indexOf(item) > -1) {
			var rate = amount / (1 + (taxInfo[zip].nonfuel / 100))
			setLineValue(rec,line,'amount',rate)
			setLineValue(rec,line,'taxrate1',taxInfo[zip].nonfuel)
		}
	}

	function setLineValue(rec,line,fieldId,value) {
		rec.setSublistValue({
			sublistId: 'item',
			fieldId: fieldId,
			line: line,
			value: value
		}) 
	}

	function getServiceZips(rec) {
		var zips = []
		var lineCount = rec.getLineCount('item')

		for (var a = 0; a < lineCount; a++) {
			var zip = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_service_zip',
				line: a
			})
			var amount = rec.getSublistValue({
				sublistId: 'item',
				fieldId: 'amount',
				line: a
			})
			if (defaults.isCreate == true) {
				rec.setSublistValue({
					sublistId:'item',
					fieldId: 'custcol_tax_included_amt',
					line: a,
					value: amount
				})
			}
			if (zips.indexOf(zip) == -1) {
				zips.push(zip)
			}
		}
		return zips
	}

	return {
		beforeSubmit: beforeSubmit
	}
})